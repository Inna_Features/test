;

$(document).ready(function(){

    function getSubProperty() {
        $.ajax({
            url: '/api/category/' + id, // id
            type: 'GET',
            success: function (response) {
                for (var i = 0; i < response.length; i++){
                     $('#table-body').append(
                        '<tr id="' +  response[i].id + '">' +
                            // '<th scope="row">' + response[i].id + '</th>' +
                            '<td class="property">' + response[i].property + '</td>' +
                            '<td class="short_descriptions">' + response[i].short_descriptions + '</td>' +
                            '<td><button class="C btn-default" data-id="' + response[i].id + '"> Show </button></td>' +
                        '</tr>'
                    );
                }
            },
            error: function(response) {
             alert(response.responseJSON.detail);
            }
        });
    }

    getSubProperty();

});