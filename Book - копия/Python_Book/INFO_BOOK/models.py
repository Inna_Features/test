from django.db import models


class Category(models.Model):
    category_name = models.CharField(max_length=30)
    short_description = models.CharField(max_length=100)

    def __str__(self):
        return self.category_name


class Property(models.Model):
    property_name = models.CharField(max_length=30)
    short_description = models.CharField(max_length=100)
    full_description = models.TextField()
    category = models.ForeignKey(Category, related_name='properties')

    def __str__(self):
        return self.property_name


# class User(models.Model):
#     username = models.CharField(verbose_name='username', max_length=50)
#     password = models.CharField(max_length=250)
#     email = models.EmailField(verbose_name='email address', unique=True)
#     first_name = models.CharField(verbose_name='first name', max_length=30, blank=True)
#     last_name = models.CharField(verbose_name='last name', max_length=30, blank=True)
#
#     def __str__(self):
#         return self.username


