from rest_framework import viewsets
from rest_framework.filters import DjangoFilterBackend
from ..api.serialaizers import CategorySerializer, PropertySerializer
    # UserSerializer, UserLoginSerializer, UserRegSerializer
from ..models import Category, Property
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class PropertyViewSet(viewsets.ModelViewSet):
    queryset = Property.objects.all()
    serializer_class = PropertySerializer


# class UserViewSet(viewsets.ModelViewSet):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
#     filter_backends = (DjangoFilterBackend,)
#     filter_fields = ('username',)


# class UserLoginViewSet(viewsets.ModelViewSet):
#     queryset = User.objects.all()
#     serializer_class = UserLoginSerializer
#
#     def post(self, request, *args, **kwargs):
#         data = request.data
#         serializer = UserLoginSerializer(data=data)
#         if serializer.is_valid(raise_exception=True):
#             new_data = serializer.data
#             return Response(new_data, status=HTTP_200_OK)
#         return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


# class UserRegViewSet(viewsets.ModelViewSet):
#     queryset = User.objects.all()
#     serializer_class = UserRegSerializer