from django.contrib.auth.hashers import make_password
from rest_framework.serializers import ModelSerializer, CharField
from ..models import Category, Property


# class ShortPropertySerializer(ModelSerializer):
#     class Meta:
#         model = Property
#         fields = [
#             'id',
#             'property_name',
#             'short_description',
#         ]

class PropertySerializer(ModelSerializer):
    class Meta: #(ShortPropertySerializer.Meta):
        model = Property
        fields = [
            'id',
            'property_name',
            'short_description',
            'full_description',
        ]


class CategorySerializer(ModelSerializer):
    category_properties = PropertySerializer(read_only=True, source='properties', many=True)

    class Meta:
        model = Category
        fields = [
            'id',
            'category_name',
            'short_description',
            'category_properties',
        ]


# class UserRegSerializer(ModelSerializer):
#
#     def validate_password(self, data: str) -> str:
#         return make_password(data)
#
#     class Meta:
#         model = User
#         fields = ('id', 'username', 'password', 'email', 'first_name', 'last_name')
#         extra_kwargs = {'password': {'write_only': True},
#                         'id' : {'read_only': True},}


# class UserSerializer(ModelSerializer):
#     class Meta:
#         model = User
#         fields = ('id', 'username', 'password', 'email', 'first_name', 'last_name')
#         extra_kwargs = {'password': {'write_only': True},
#                         'id' : {'read_only': True},}


# class UserLoginSerializer(ModelSerializer):
#     token = CharField(allow_blank=True, read_only=True)
#     username = CharField()
#
#     class Meta:
#         model = User
#         fields = ('username', 'password', 'token', )
#         extra_kwargs = {'password': {'write_only': True}, }
#
#     def validate(self, data):
#         return data
