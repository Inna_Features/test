from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from ..api.viewsets import CategoryViewSet

    # UserViewSet, UserLoginViewSet, UserRegViewSet

router = DefaultRouter()
router.register(r'category', CategoryViewSet)
# router.register(r'property', PropertyViewSet)
# router.register(r'user', UserViewSet)
# router.register(r'login', UserLoginViewSet)
# router.register(r'register', UserRegViewSet)

# Подключение маршрутов Urls
urlpatterns = [
    url(r'^', include(router.urls)),
]